import { environment } from '../../environments/environment';
import { MockCategoriesService } from './categories.service.mock';
import { CategoriesService } from './categories.service';

export const EnvCategoriesService = environment.mockedServices ?
  { provide: CategoriesService, useValue: MockCategoriesService } : CategoriesService;
