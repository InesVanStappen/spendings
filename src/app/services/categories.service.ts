import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/Observable/of';


@Injectable()
export class CategoriesService {

	constructor(public http: HttpClient) {
	}

	getCategoriesIncome(): Observable<string[]> {
		const url = environment.api + `categories/income`;
		return this.http.get<string[]>(url);
	}

	getCategoriesSpending(): Observable<string[]> {
		const url = environment.api + `categories/spending`;
		return this.http.get<string[]>(url);
	}
}
