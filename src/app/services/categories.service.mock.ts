import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

export const MockCategoriesService = {
	getCategoriesSpending: (): Observable<String[]> => {
		return of(
			[
				'Cat1',
				'Cat2',
				'Cat3',
				'Cat4',
				'Cat5',
			]
		);
	},
	getCategoriesIncome: (): Observable<String[]> => {
		return of(
			[
				'Cat1',
				'Cat2',
				'Cat3',
				'Cat4',
				'Cat5',
			]
		);
	}
};
