import { MockCategoriesService } from './categories.service.mock';

export const CategoriesServiceSpy = {
	getCategoriesIncome: jasmine.createSpy('getCategoriesIncome', MockCategoriesService.getCategoriesIncome).and.callThrough(),
	getCategoriesSpending: jasmine.createSpy('getCategoriesSpending', MockCategoriesService.getCategoriesSpending).and.callThrough(),
};
