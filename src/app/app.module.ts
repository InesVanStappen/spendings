import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { CategoriesSandbox } from './sandboxes';
import { EnvCategoriesService } from './services';
import { SideNavComponent } from './side-nav/side-nav.component';

const CATEGORIES_SERVICE_AND_SANDBOX = [
	CategoriesSandbox,
	EnvCategoriesService
];

@NgModule({
	declarations: [
		AppComponent,
		SideNavComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule
	],
	providers: [
		...CATEGORIES_SERVICE_AND_SANDBOX
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
