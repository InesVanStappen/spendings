import { Injectable } from '@angular/core';
import { CategoriesService } from '../services/categories.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class CategoriesSandbox {

	constructor(
		private categoriesService: CategoriesService,
	) { }

	getCategoriesIncome(): Observable<string[]> {
		return this.categoriesService.getCategoriesIncome();
	}

	getCategoriesSpending(): Observable<string[]> {
		return this.categoriesService.getCategoriesSpending();
	}
}
